package sda.jdbc;

import lombok.extern.slf4j.Slf4j;
import sda.jdbc.context.AppContext;
import sda.jdbc.domain.Klient;

@Slf4j
public class Main {
    private AppContext appContext;

    public static void main(String[] args) {
        new Main().runApp();
    }

    public void runApp() {
        appContext = new AppContext();

        // TODO: dopisac brakujace ciala metod, zeby zapis sie powiodl
        Klient klient = appContext.getKlientService().zapiszNowegoKlienta("Szymon", "Dembek");

        // TODO: pobranie istniejacej ksiazki i utworzenie jej oceny przez nowego klienta

        // TODO: utworzenie nowej ksiazki i utworzenie jej oceny przez nowego klienta
    }
}
