package sda.jdbc.context;

import sda.jdbc.repository.KlientRepository;
import sda.jdbc.service.ConnectionProvider;
import sda.jdbc.service.KlientService;

public class AppContext {

    private final ConnectionProvider connectionProvider;
    private final KlientService klientService;
    private final KlientRepository klientRepository;

    public AppContext() {
        connectionProvider = new ConnectionProvider();

        klientRepository = new KlientRepository(connectionProvider);

        klientService = new KlientService(klientRepository);
    }

    public KlientService getKlientService() {
        return klientService;
    }
}
