package sda.jdbc.repository;

import sda.jdbc.domain.Klient;
import sda.jdbc.service.ConnectionProvider;

public class KlientRepository {

    private final ConnectionProvider connectionProvider;

    public KlientRepository(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public Klient create(Klient klient) {
        return null;
    }

    public Klient read(long id) {
        return null;
    }

    public void update(Klient klient) {

    }

    public void delete(Klient klient) {

    }

    public void delete(long id) {

    }
}
