package sda.jdbc.service;

import java.sql.Connection;

public class ConnectionProvider {

    private final Connection connection;

    public ConnectionProvider() {
        connection = null;
    }

    public Connection getConnection() {
        return connection;
    }
}
