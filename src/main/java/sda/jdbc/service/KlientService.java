package sda.jdbc.service;

import sda.jdbc.domain.Klient;
import sda.jdbc.repository.KlientRepository;

public class KlientService {

    private final KlientRepository klientRepository;

    public KlientService(KlientRepository klientRepository) {
        this.klientRepository = klientRepository;
    }

    public Klient zapiszNowegoKlienta(String imie, String nazwisko) {
        Klient klient = new Klient();

        return klientRepository.create(klient);
    }
}
